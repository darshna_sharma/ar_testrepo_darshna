/*
    Author          : Daniel Measures (SFDC) 
    Date Created    : 26/10/2010
    Description     : Class utilised by triggers OpportunityBeforeInsert and OpportunityBeforeUpdate.
                        Custom field TECH_OwnerId__c populated with the Opportunities ownerId.
    02-Apr-2013   Srinivas Nallapati   Apr13 PRM  Alert Partner Involvement  
    11-Feb-2013   Srinivas Nallapati   Apr14 PRM  validate Partner Involvement 

*/
public without sharing class AP01_OpportunityFieldUpdate {
    
    public static boolean blOpportunityTestClassFlag = true;
    public static boolean isOTMupdatedToSendMail = false;
    public static void populateTechOwnerId(List<Opportunity> opps){
        
//        if(blOpportunityTestClassFlag)
//        {
        for(Opportunity o : opps){
            o.TECH_OwnerID__c = o.OwnerId;
            //o.Amount=0;
    

    





        }
//        }
    }  

    //Update the technical field "amount in euro"
    /*public static void UpdateTECHAmountEUR(List<Opportunity> opps){
        try{
            if(opps!=null && !opps.isEmpty()){
            /*
                Map <String, double> IsoCode_Rate = new Map <String, double>();
                List<DatedConversionRate> CurrencyList= new List<DatedConversionRate>();
                Set<String> currencySet = new Set<String>();
                double rate;
              */  
                /*for(Opportunity o : opps){
                    o.TECH_AmountEUR__c=Utils_Methods.convertCurrencyToEuroforDatedCurrency(o.CurrencyIsoCode , o.Amount);
//                    currencySet.add(o.CurrencyIsoCode);
                }
               /*
                CurrencyList= [SELECT ConversionRate, IsoCode FROM DatedConversionRate WHERE IsoCode IN: currencySet];
                
                for(DatedConversionRate cT : CurrencyList){
                    IsoCode_Rate.put(cT.IsoCode, cT.ConversionRate);
                }
              
                for(Opportunity o : opps){
                    rate = IsoCode_Rate.get(o.CurrencyIsoCode);
                    o.TECH_AmountEUR__c = o.Amount / rate;                    
                }
                
            }
        }
        catch(Exception e){
            system.debug('Exception: ' + e);
        }
    }*/
    
    public static void updateTechIsLevel2(List<Opportunity> opps){
        try{
            if(opps!=null && !opps.isEmpty()){
                List <Schema.PicklistEntry> picklistValues = Opportunity.OpportunityScope__c.getDescribe().getPicklistValues();
                for(Opportunity o : opps){
                    String scope = '';                                                                
                    For(Schema.PicklistEntry pE : picklistValues) {                
                        if(pE.getValue() == o.OpportunityScope__c) {
                            scope = pE.getLabel();                    
                            break;                
                        }            
                    }

                    if(scope.contains('LEVEL 2') || scope.contains('LEVEL2'))
                        o.TechIsLevel2__c = True;
                    else
                        o.TechIsLevel2__c = False;
                }
            }
        }
        catch(Exception e){
            system.debug('Exception: ' + e);
        }
    }
    
    /*
    public static void updateOpportunityAssociated(List<Opportunity> opps){
          if(blOpportunityTestClassFlag  = true)
          {
           
          List<OppAgreement__c> lstOppsAgree = null;
          OppAgreement__c objAgree = null;
          List<Opportunity> lstOpps = null;
          lstOpps = [SELECT o.AgreementReference__c, o.AgreementReference__r.Id, o.Id 
          FROM Opportunity o WHERE AgreementReference__c =: opps[0].AgreementReference__c];
          if(lstOpps.size() !=0)
          {
              if(lstOpps[0].ID == opps[0].ID)
              {
                   lstOppsAgree = [SELECT ID,OpportunityAssociated__c 
                   FROM OppAgreement__c WHERE ID=:opps[0].AgreementReference__c];
                     
                   for(OppAgreement__c oa : lstOppsAgree)
                   {
                         oa.OpportunityAssociated__c = false;  
                   } 
              }
              update lstOppsAgree;
          }    
          }
         
          
    }
    */
    //Srinivas Nallapati    Apr13 PRM  2/Apr/13
    // Anil Sistla          Oct13 PRM - Opportunity Passed to Partner & related BRs
    public static void checkOpportunityPassedToPartner(List<Opportunity> lsOpp) {
        if(isOTMupdatedToSendMail == false) {
           isOTMupdatedToSendMail = true;
        List<OPP_ValueChainPlayers__c> lsVCP = new List<OPP_ValueChainPlayers__c>();

        //lsVCP = [select Contact__c, ContactRole__c, OpportunityName__c from OPP_ValueChainPlayers__c where OpportunityName__c in :lsVCP AND OpportunityName__r.PartnerInvolvement__c='Passed to Partner' ]
        List<Opportunity> lsOpp1 = new List<Opportunity>();
        if(!(lsOpp.isEmpty()))
       
       /* lsOpp1 = [SELECT PartnerInvolvement__c, Name,
                                    (SELECT UserId, OpportunityId, User.ContactId  FROM OpportunityTeamMembers WHERE User.ContactId != null AND User.isActive = true AND OpportunityTeamMember.TeamMemberRole = :System.Label.CLOCT13PRM13),
                                    (SELECT name, Contact__C, ContactRole__c from Value_Chain_Players__r where Contact__c != null and ContactRole__c = :System.Label.CLMAY13PRM40 ) FROM Opportunity WHERE id IN :lsOpp];
        */
        
        //START: Release Jul14, BR-5657
        lsOpp1 = [SELECT PartnerInvolvement__c, Name,
                                    (SELECT UserId, OpportunityId, User.ContactId  FROM OpportunityTeamMembers WHERE User.ContactId != null AND User.isActive = true AND OpportunityTeamMember.TeamMemberRole = :System.Label.CLOCT13PRM13),
                                    (SELECT name, Contact__C, ContactRole__c from Value_Chain_Players__r 
                                      where (Contact__c != null and ContactRole__c = :System.Label.CLMAY13PRM40) OR (Account__c !=null and AccountRole__c = :System.Label.CLJUL14PRM03)
                                    ) 
                    FROM Opportunity WHERE id IN :lsOpp];
        //END: Release Jul14, BR-5657
                                    
        
        Map<Id, Opportunity> originalOppMap = new Map<Id, Opportunity>(lsOpp);
        Set<Id> withVCPTeam = new Set<Id>();
        Set<Id> originalOppMapId = originalOppMap.keySet();
        
        //Apr14
        set<id> conIds = new set<id>();
        for(Opportunity top : lsOpp1)
        {
            for(OpportunityTeamMember totm : top.OpportunityTeamMembers)
            {
                conIds.add(totm.User.ContactId);
            }
        }

        set<id> featureCons = new set<id>();
        //Add Feature ID in the SOQL where condition
        List<ContactAssignedFeature__c> lstConFea = [SELECT Active__c,Contact__c,FeatureCatalog__c FROM ContactAssignedFeature__c WHERE Active__c = true AND Contact__c IN :conIds AND FeatureCatalog__r.Category__c includes (:System.Label.CLAPR14PRM51)];
        for(ContactAssignedFeature__c conf : lstConFea)
        {
            featureCons.add(conf.Contact__c);
        }
        //end apr14       
        // Create a set of Opportunity ID from OpportunityTeamMembers
        // Create a set of Opportunity ID from ValueChainPlayer
        // Create a union of Opportunity ID from above two sets
        // Remove this union from Original list of Opportunities, and add error to the remaining list
        
        List<OpportunityTeamMember>  lstOTMtoSendMail  = new List<OpportunityTeamMember>();
        List<Task> lstPartnerTasks = new List<Task>();
        List<PartnerOpportunityStatusHistory__c> lstPartnerOpptyTasks = new List<PartnerOpportunityStatusHistory__c>();
        List<OPP_ValueChainPlayers__c> lstPartnerVCP = new List<OPP_ValueChainPlayers__c>();
        
        for(Opportunity op : lsOpp1) {
            if ((op.OpportunityTeamMembers != null && op.OpportunityTeamMembers.size() > 0) && 
                (op.Value_Chain_Players__r != null && op.Value_Chain_Players__r.size() > 0)) {

                
                for(OpportunityTeamMember otm : op.OpportunityTeamMembers) {
                    //Apr14
                    if(featureCons.contains(otm.User.ContactId))
                    {
                        withVCPTeam.add(op.Id);
                        lstPartnerTasks.add(AP_OpportunityTeamMember.createTask(op, otm.UserId));
                        lstPartnerOpptyTasks.add((new PartnerOpportunityStatusHistory__c(Opportunity__c = op.Id, User__c = otm.UserId, Status__c = 'Assigned')));
                        lstPartnerVCP.add((new OPP_ValueChainPlayers__c(OpportunityName__c = op.id, Contact__c = otm.User.ContactId, ContactRole__c = System.Label.CLSEP12PRM16)));
                        
                        otm.TECH_PartnerInvolvedDate__c = System.now();
                        lstOTMtoSendMail.add(otm);
                    }
                }
            }
        }

        System.Debug('Original Map: ' + originalOppMap);
        System.Debug('Retrieved Map: ' + withVCPTeam);
        originalOppMapId.removeAll(withVCPTeam);
        System.Debug('Modified Set: ' + originalOppMapId);
        if (originalOppMapId != null && originalOppMapId.size() > 0) {
            for (String oppId : originalOppMapId) {
                originalOppMap.get(oppId).addError(System.Label.CLOCT13PRM06);
            }
        }

        if (lstPartnerTasks.size() > 0)
            insert lstPartnerTasks;
        if (lstPartnerOpptyTasks.size() > 0)
            insert lstPartnerOpptyTasks;
        if (lstPartnerVCP.size() > 0)
            insert lstPartnerVCP;
        if(lstOTMtoSendMail.size() > 0)
            update lstOTMtoSendMail;

        }//End of if
    }//ENd of method
    
    public static void updateOppLines(Map<Opportunity, String> oppStgs)
    {
        List<String> stgs = oppStgs.values();
        List<Id> oppIds = new List<Id>();
        List<OPP_ProductLine__c> prodLnsUpd = new List<OPP_ProductLine__c>();
        for(Opportunity o: oppStgs.keyset())        
        {
            oppIds.add(o.Id);            
        }
        //Queries the Product Lines related to the Opportunity
        List<OPP_ProductLine__c> prodLns = new List<OPP_ProductLine__c>();
        if(!(oppIds.isEmpty()))
            prodLns = [select Opportunity__c , LineStatus__c from OPP_ProductLine__c where Opportunity__c in: oppIds and (LineStatus__c!=:Label.CL00139)];
        
        for(Opportunity o: oppStgs.keyset())        
        {
            for(OPP_ProductLine__c prod: prodLns)                
            {
                //Checks if the Opportunity Stage is Stage 0 and the Status is Account Not selected or Lost
                if(oppStgs.get(o) == 'stge0StatsLost')            
                {
                    //Checks if the Line Status!= to be deleted and updates the Line Status to Lost
                    //Checks if the Line Status!= Service Contract Amendment and updates the Line Status to Lost for June Release BR-9826
                    if(prod.Opportunity__c == o.Id && prod.LineStatus__c!= Label.CL00139 && prod.LineStatus__c!=Label.CLJUN16SLS01)
                    {                     
                        prod.LineStatus__c = Label.CLSEP12SLS14;
                        prodLnsUpd.add(prod);
                    }
                }
                //Checks if the Opportunity is in Stage 0 and Status equals Cancelled
                if(oppStgs.get(o) == 'stge0StatsCanlld')            
                {
                    //Checks if the Line Status!= to be deleted and updates the Line Status to Canclled
                    //Checks if the Line Status!= Service Contract Amendment and updates the Line Status to Cancelled for June Release BR-9826
                    if(prod.Opportunity__c == o.Id && prod.LineStatus__c!= Label.CL00139 && prod.LineStatus__c!=Label.CLJUN16SLS01)
                    {
                        prod.LineStatus__c = Label.CL00326;
                        prodLnsUpd.add(prod);                        
                    }                              
                }
                //Checks if the Opportunity is in Stage 7 and Status equals Won
                if(oppStgs.get(o) == 'stge7StatsWon')            
                {
                        //Checks if the Line Status equals Pending and updates the Line Status to Won
                        if(prod.Opportunity__c == o.Id && prod.LineStatus__c== Label.CLSEP12I2P84)
                        {
                            prod.LineStatus__c = Label.CLOCT13SLS05;
                            prodLnsUpd.add(prod);                            
                        }                            
                }
                
				//Checks if the LineStatus is service contract amendment and updates it to 'to be deleted' for JUNE Release BR-9826
				if(oppStgs.get(o) == 'stge0StatsLost' || oppStgs.get(o) == 'stge0StatsCanlld' ||oppStgs.get(o) == 'stge7StatsWon')
				{
					if(prod.Opportunity__c == o.Id && prod.LineStatus__c== Label.CLJUN16SLS01)
					{
					prod.LineStatus__c = Label.CLJUN16SLS13;
					prodLnsUpd.add(prod);
					}
				}
				//end june 2016 Release BR-9826
            }
        }

        //udpates the Product Lines
        if(!(prodLnsUpd.IsEmpty()))
            update prodLnsUpd;

    }
    
    /* Modified for BR-5894 */
    //Updates the Forecast category to Omitted, when the Included in forecast is set as "NO"
    public static void populateForecast(List<Opportunity> opptys)
    {
        for(Opportunity opp: opptys)    
        {
            opp.ForecastCategoryName = Label.CLOCT14SLS15;            
        }
        
    }
    /* End of modification for BR-5894 */ 
    /* March 2016 release - BR-8430
    Prevent changes on Completed Master Project
    Prevent User linking the Opportunity to the hierarchy if the Master Project is completed */
    
     public static void masterProjectStatusCheck(List<Opportunity> opptys)
    {
       Map<Id, Opportunity> opptiesMap = new Map<Id, Opportunity>(opptys);
       System.debug('>>>>>opptys'+opptys);
        List<Opportunity> oppties=[Select Id,Project__c,Project__r.ProjectStatus__c,ParentOpportunity__c,ParentOpportunity__r.Project__c,ParentOpportunity__r.Project__r.ProjectStatus__c,ParentOpportunity__r.ParentOpportunity__c,ParentOpportunity__r.ParentOpportunity__r.Project__c,ParentOpportunity__r.ParentOpportunity__r.Project__r.ProjectStatus__c from Opportunity where id in:opptys];
        for(Opportunity opp: opptys)
            opptiesMap.put(opp.Id,opp);
        System.debug('>><<>><oppties'+oppties);
        System.debug('>><<>><opptiesMap'+opptiesMap);
        for(Opportunity opp: oppties)    
        {
             System.debug('>>>>>opp.Project__c'+opp.Project__c+'>>>>opp.ParentOpportunity__c'+opp.ParentOpportunity__c+'>>>>>>opp.ParentOpportunity__r.ParentOpportunity__c'+opp.ParentOpportunity__r.ParentOpportunity__c);
             //For Object Opportunity
            if(opp.Project__c!=null){
                 if(opp.Project__r.ProjectStatus__c=='Completed')
                      opptiesMap.get(opp.ID).addError(Label.CLMAR16SLS12);
             }
            //For simple Opportunity
             if(opp.ParentOpportunity__c!=null){
                 if(opp.ParentOpportunity__r.Project__c!=null){
                     if(opp.ParentOpportunity__r.Project__r.ProjectStatus__c=='Completed')
                          opptiesMap.get(opp.ID).addError(Label.CLMAR16SLS12);
                 }
             }
            //For the existing Opportunities having Master-Object-Simple hierarchy
            if(opp.ParentOpportunity__r.ParentOpportunity__c!=null){
               if(opp.ParentOpportunity__r.ParentOpportunity__r.Project__c!=null){
                     if(opp.ParentOpportunity__r.ParentOpportunity__r.Project__r.ProjectStatus__c=='Completed')
                         opptiesMap.get(opp.ID).addError(Label.CLMAR16SLS12);
                }
           }           
        }
        
    }   
}